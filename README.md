# likelion-hospital-review

## 📌 공식 문서

👉 Spring Security : https://docs.spring.io/spring-authorization-server/docs/current/reference/html/getting-started.html

👉 JWT : https://jwt.io/

<br />

<br />

## 📒 참고 자료

👉 스프링부트 핵심가이드 -스프링 부트를 활용한 애플리케이션 개발 실무- 

<br />

<br />

## 🛠 Dev Tool & Stack

- IntelliJ IDEA  2022.2.3
  - Spring Boot
  - JPA 
  - Spring Security
  - Spring Security Test
  - JWT
  - Lombok
  - MySQL Driver
- MySql 8.0
- Talend API Tester

<br />

<br />

## 📜 설계 문서

https://docs.google.com/document/d/158WMhQ2xvETVnTimGMkrEh5I6LGxWaa33RSirJpUEAs/edit

- 요구사항 정의서

<br />

<br />

## ✒ 서비스 아키텍처

![image-20221209173534792](./assets/image-20221209173534792.png)

<br />

<br />

## 🧩 ER 다이어그램

![image-20221209170426270](./assets/image-20221209170426270.png)

<br />

<br />

## 💻 구현 기능

|           기능           |                            Note                            |        End Point         |  ex  |
| :----------------------: | :--------------------------------------------------------: | :----------------------: | :--: |
|         회원가입         |               [회원가입](./note/회원가입.md)               | POST /api/v1/users/join  |      |
|      Exception 처리      |          [Exception처리](./note/Exception처리.md)          |                          |      |
|  UserRestControllerTest  | [UserRestControllerTest](./note/UserRestControllerTest.md) |                          |      |
|   Spring Security 적용   |        [Spring Security](./note/SpringSecurity.md)         |                          |      |
|          로그인          |                  [Login](./note/Login.md)                  | POST /api/v1/users/login |      |
| JWT로 접근 권한 인증하기 |           [JWT 권한 부여](./note/JWT이용하기.md)           |                          |      |
|        댓글 등록         |            [Review 등록](./note/Review등록.md)             |  POST / api/v1/reviews   |      |
