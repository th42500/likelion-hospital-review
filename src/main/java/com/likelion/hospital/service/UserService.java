package com.likelion.hospital.service;
import com.likelion.hospital.domain.User;
import com.likelion.hospital.exception.AppException;
import com.likelion.hospital.exception.ErrorCode;
import com.likelion.hospital.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository ur;
    private final BCryptPasswordEncoder encoder;

    public String join(String userName, String password) {

        ur.findByUserName(userName)
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.USERNAME_DUPLICATED, userName+"는 이미 있습니다.");
                });

        User user = User.builder()
                .userName(userName)
                .password(encoder.encode(password))
                .build();
        ur.save(user);

        return "SUCCESS";
    }

}
